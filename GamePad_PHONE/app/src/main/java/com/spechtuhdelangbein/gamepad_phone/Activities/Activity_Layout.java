package com.spechtuhdelangbein.gamepad_phone.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.spechtuhdelangbein.gamepad_phone.Configuration.Configuration_Others;
import com.spechtuhdelangbein.gamepad_phone.Utilities.HelperFunctions;
import com.spechtuhdelangbein.gamepad_phone.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

/**
 * Created by Constantin on 09.08.2015.
 */
public class Activity_Layout extends Activity {

    ListView list;



    @Override
    protected void onCreate(Bundle savedInstanceState){

        //paths to the layout and thumbnail folders
        File layoutPath = new File(Configuration_Others.layoutDirectory);
        File thumbnailPath = new File(Configuration_Others.thumbnailDirectory);

        setLanguage();

        //save folder contents into file arrays and sort the arrays
        File[] layoutFiles = layoutPath.listFiles();
        Arrays.sort(layoutFiles);
        File[] thumbnailFiles = thumbnailPath.listFiles();
        Arrays.sort(thumbnailFiles);

        //write the arrays into String and Bitmap ArrayLists
        final ArrayList<String> layoutNames=new ArrayList<>();
        for(int i=0; i< layoutFiles.length;i++){
            layoutNames.add(i, layoutFiles[i].getName());
        }
        ArrayList<Bitmap> layoutImages= new ArrayList<>();
        for(int i=0; i< thumbnailFiles.length;i++){
            layoutImages.add(i, BitmapFactory.decodeFile(thumbnailFiles[i].getAbsolutePath()));
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout);

        //Setup the Custom Listview
        CustomListAdapter adapter = new CustomListAdapter(this,layoutNames,layoutImages);
        list=(ListView)findViewById(R.id.layout_list);
        list.setAdapter(adapter);

        //OnItemClick gets the clicked layout name and sets it in sharedPrefs as selected layout
       list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               String selectedItem = layoutNames.get(+position);
               SharedPreferences sharedPrefs = getSharedPreferences(Configuration_Others.LAYOUT_PREFS, 0);
               SharedPreferences.Editor editor = sharedPrefs.edit();
               editor.putString(Configuration_Others.SELECTED_LAYOUT, selectedItem);
               editor.commit();
               Toast.makeText(getApplicationContext(), selectedItem + " " + getResources().getString(R.string.layout_activated), Toast.LENGTH_SHORT).show();
           }
       });

        //OnItemLongClickListener starts an AlertDialog which asks if the listentry should be deleted
        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

                final String selectedItem = layoutNames.get(+position);

                //Create new AlertDialog and set displayed Strings
                AlertDialog.Builder adb = new AlertDialog.Builder(Activity_Layout.this);
                adb.setTitle(getResources().getString(R.string.layout_confirm_delete_title));
                adb.setMessage(getResources().getString(R.string.layout_confirm_delete_msg) +" "+ selectedItem);


                adb.setNegativeButton(getResources().getString(R.string.layout_confirm_delete_cancel), null);
                adb.setPositiveButton(getResources().getString(R.string.layout_confirm_delete_ok), new AlertDialog.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences sharedPrefs = getSharedPreferences(Configuration_Others.LAYOUT_PREFS, 0);
                        if (HelperFunctions.isExternalStorageWritable()&&!(selectedItem.equals(Configuration_Others.layoutStandard))) {
                            Log.d("selected item ",selectedItem);
                            CustomListAdapter adapter = (CustomListAdapter)list.getAdapter();
                            HelperFunctions.deleteLayout(selectedItem);
                            adapter.remove(position);
                            adapter.notifyDataSetChanged();


                            if (selectedItem.equals(sharedPrefs.getString(Configuration_Others.SELECTED_LAYOUT, Configuration_Others.layoutStandard))){
                                SharedPreferences.Editor editor = sharedPrefs.edit();
                                editor.putString(Configuration_Others.SELECTED_LAYOUT, Configuration_Others.layoutStandard);
                                editor.commit();
                            }

                        } else {
                            Toast.makeText(getApplicationContext(),selectedItem+getResources().getString(R.string.layout_confirm_delete_failed), Toast.LENGTH_SHORT).show();
                        }

                    }
                });
                AlertDialog ad = adb.show();
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        setLanguage();
        super.onResume();
    }

    private class CustomListAdapter extends ArrayAdapter<String>{
        private final Activity context;
        private final ArrayList<String> itemName;
        private final ArrayList<Bitmap> imgBitmap;

        /**
         * Custom List Adapter which displays Text and a Bitmap
         * @param context Activity context
         * @param itemName ArrayList of all given String Name entries
         * @param imgBitmap ArrayList of all corresponding thumbnails
         */

        public CustomListAdapter(Activity context,ArrayList<String> itemName, ArrayList<Bitmap> imgBitmap){
            super(context,R.layout.layout_entry,itemName);

            this.context=context;
            this.itemName=itemName;
            this.imgBitmap=imgBitmap;
        }

        /**
         * Returns a rowView which contains one textfield and image for a Layout
         * Uses R.layout.layout_entry
         * @param position The position in the List
         * @param view
         * @param parent
         * @return Returns the rowView
         */
        public View getView(int position,View view,ViewGroup parent){
            LayoutInflater inflater=context.getLayoutInflater();
            View rowView=inflater.inflate(R.layout.layout_entry, null, true);

            TextView itemText =(TextView) rowView.findViewById(R.id.layout_item);
            ImageView imageView=(ImageView) rowView.findViewById(R.id.layout_icon);

            itemText.setText(itemName.get(position));
            imageView.setImageBitmap(imgBitmap.get(position));
            return rowView;
        }
        /**
         * Removes image and text entry at given position
         * @param position the position of the entry
         */
        public void remove(int position){
            itemName.remove(position);
            imgBitmap.remove(position);
        }
    }
    public void setLanguage() {
        Log.d("Milestone", "Setting language");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String languageKey = "com.spechtuhdelangbein.gamepad_phone.language";
        String language = prefs.getString(languageKey, "NULL");

        if(language.equals("NULL")) {
            Log.d("Milestone", "No language found");
            return;
        }
        else {
            Log.d("Milestone", "Setting language to "+language);
            Locale myLocale = new Locale(language);
            Resources res = getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            conf.locale = myLocale;
            res.updateConfiguration(conf, dm);
        }
    }
}
