package com.spechtuhdelangbein.gamepad_phone.Utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.NotificationCompat;

import com.spechtuhdelangbein.gamepad_phone.Connection.ConnectionLobby;
import com.spechtuhdelangbein.gamepad_phone.R;

import java.util.ArrayList;

/**
 * Created by Andreas on 14.06.2015.
 */
/*
* This class posts Notifications to your phone and is callable from other classes with
* Context. The Notification shows the Gazoo-Logo, the project name and the current calling class
* */
public class Helper_NotificationClass {

    public ArrayList<Integer> UsedMsgIDs = new ArrayList<Integer>();

    public void Notify(Context cxt,int MsgID)
    {
        int mId = MsgID;
        UsedMsgIDs.add(MsgID);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(cxt)
                        .setSmallIcon(R.drawable.gazoo_small)
                        .setContentTitle("Gazoo GamePad")
                        .setContentText(cxt.getClass().getSimpleName());
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(cxt);
        stackBuilder.addParentStack(ConnectionLobby.class);

        NotificationManager mNotificationManager =
                (NotificationManager) cxt.getSystemService(cxt.NOTIFICATION_SERVICE);
        mNotificationManager.notify(mId, mBuilder.build());

    }

    public void Notify(Context cxt,String msg,int MsgID)
    {
        int mId = MsgID;
        UsedMsgIDs.add(MsgID);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(cxt)
                        .setSmallIcon(R.drawable.gazoo_small)
                        .setContentTitle("Gazoo GamePad")
                        .setContentText(msg);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(cxt);
        stackBuilder.addParentStack(ConnectionLobby.class);

        NotificationManager mNotificationManager =
                (NotificationManager) cxt.getSystemService(cxt.NOTIFICATION_SERVICE);
        mNotificationManager.notify(mId, mBuilder.build());

    }

    public void Notify(Context cxt,String msg,String title,int MsgID)
    {
        int mId = MsgID;
        UsedMsgIDs.add(MsgID);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(cxt)
                        .setSmallIcon(R.drawable.gazoo_small)
                        .setContentTitle(title)
                        .setContentText(msg);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(cxt);
        stackBuilder.addParentStack(ConnectionLobby.class);

        NotificationManager mNotificationManager =
                (NotificationManager) cxt.getSystemService(cxt.NOTIFICATION_SERVICE);
        mNotificationManager.notify(mId, mBuilder.build());

    }

    public void ShowAlertDialog(final Context cxt)
    {
        AlertDialog alertDialog = new AlertDialog.Builder((Activity)cxt).create(); //Read Update
        alertDialog.setTitle("We're sorry");
        alertDialog.setMessage("Your device doesn't seem to support Bluetooth at all. \n This application requires bluetooth to communicate with your tablet");

        alertDialog.setButton("Take me back", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ((Activity) cxt).finish();
                System.exit(0);
            }
        });

        alertDialog.show();

    }

    public void RemoveNotification(Activity ctxt,int MsgID)
    {
        NotificationManager notificationManager = (NotificationManager)ctxt.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(MsgID);
    }

    public void RemoveAllNotifications(Activity ctxt)
    {
        for(Integer i : UsedMsgIDs)
        {
            RemoveNotification(ctxt,i);
        }
        UsedMsgIDs.clear();
    }

}
