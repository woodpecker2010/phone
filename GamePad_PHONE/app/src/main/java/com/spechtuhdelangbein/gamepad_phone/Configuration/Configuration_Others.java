package com.spechtuhdelangbein.gamepad_phone.Configuration;

import android.os.Environment;

import com.spechtuhdelangbein.gamepad_phone.Utilities.CommandHandler;
import com.spechtuhdelangbein.gamepad_phone.Connection.ConnectedThread;
import com.spechtuhdelangbein.gamepad_phone.R;
import com.spechtuhdelangbein.gamepad_phone.Utilities.Helper_NotificationClass;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Andreas on 01.07.2015.
 *
 * Globally defined Objects & Variables
 */
public class Configuration_Others {

    //global variable for longPress detection in Editor (no view passed -> workaround)
    public static boolean editorLongPress = false;

    // Information about device
    /** as soon as this variable is changed it needs to be refreshed everywhere! (mind: call by value) */
    public static int playerNo;


    //Layout Directory (SD Card directory is added in the xml parser)
    public static String layoutDirectory = Environment.getExternalStorageDirectory().toString()+"/Gamepad_Layouts/";
    public static String thumbnailDirectory = Environment.getExternalStorageDirectory().toString()+"/Gamepad_Thumbnails/";
    //Standard Layout
    public static String layoutStandard = "Standard.xml";
    public static String thumbnailStandard = "Standard.png";
    public static String standardName = "Standard";

    public static final String LAYOUT_PREFS = "LayoutPreferences";
    public static final String SELECTED_LAYOUT = "SelectedLayout";

    public static final int BYTE_ARRAY_LENGTH = 13;

    //Message Types
    public final static int TYPE_GAMEPAD   = 1;
    public final static int TYPE_KEYBOARD  = 2;
    public final static int TYPE_MOUSE     = 3;
    public final static int TYPE_INFORMATION        = 4;
    public final static int TYPE_CONNECTION_REQUEST = 5;
    public final static int TYPE_QUIT_CONNECTION    = 6;
    public final static int AMOUNT_TYPES = 6;
    public final static int PLAYER_1 = 1;
    public final static int PLAYER_2 = 2;
    public final static int PLAYER_3 = 3;
    public final static int PLAYER_4 = 4;


    // This map stores correspondences between R.drawable resources and Gamepad Element IDs
    public static final Map<Integer,Integer> imageMap;
    static {
        Map<Integer,Integer> thisMap = new HashMap<Integer,Integer>();  // can be improved by using
        thisMap.put(Configuration_Gamepad.ID_BUTTON_A, R.drawable.a_button);
        thisMap.put(Configuration_Gamepad.ID_BUTTON_B,R.drawable.b_button);
        thisMap.put(Configuration_Gamepad.ID_BUTTON_X,R.drawable.x_button);
        thisMap.put(Configuration_Gamepad.ID_BUTTON_Y,R.drawable.y_button);
        thisMap.put(Configuration_Gamepad.ID_BACK,R.drawable.back_button);
        thisMap.put(Configuration_Gamepad.ID_START,R.drawable.start_button);
        thisMap.put(Configuration_Gamepad.ID_XBOX_LOGO,R.drawable.xbox_button);
        thisMap.put(Configuration_Gamepad.ID_DPAD_LEFT,R.drawable.dpad_left_button);
        thisMap.put(Configuration_Gamepad.ID_DPAD_RIGHT,R.drawable.dpad_right_button);
        thisMap.put(Configuration_Gamepad.ID_DPAD_UP,R.drawable.dpad_up_button);
        thisMap.put(Configuration_Gamepad.ID_DPAD_DOWN,R.drawable.dpad_down_button);
        thisMap.put(Configuration_Gamepad.ID_LB,R.drawable.lb_button);
        thisMap.put(Configuration_Gamepad.ID_RB,R.drawable.rb_button);
        thisMap.put(Configuration_Gamepad.ID_RIGHT_TRIGGER,R.drawable.rt_button);
        thisMap.put(Configuration_Gamepad.ID_LEFT_TRIGGER,R.drawable.lt_button);
        thisMap.put(Configuration_Gamepad.ID_LEFT_STICK_PRESS,R.drawable.stick_button);
        thisMap.put(Configuration_Gamepad.ID_RIGHT_STICK_PRESS,R.drawable.stick_button);
        thisMap.put(Configuration_Gamepad.ID_TOUCHPAD,R.drawable.touch_button);
        imageMap= Collections.unmodifiableMap(thisMap);
    }
}
