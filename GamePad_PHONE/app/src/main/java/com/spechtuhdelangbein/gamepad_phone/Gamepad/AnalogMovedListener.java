package com.spechtuhdelangbein.gamepad_phone.Gamepad;

/**
 * Created by Constantin on 22.07.2015.
 */
public interface AnalogMovedListener {
    public void OnMoved(int pan, int tilt);
    public void OnReleased();
    public void OnReturnedToCenter();
}
