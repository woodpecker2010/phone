package com.spechtuhdelangbein.gamepad_phone.Activities;

import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.spechtuhdelangbein.gamepad_phone.Configuration.Configuration_Others;
import com.spechtuhdelangbein.gamepad_phone.Utilities.EditorDragListener;
import com.spechtuhdelangbein.gamepad_phone.Gamepad.GamepadElements;
import com.spechtuhdelangbein.gamepad_phone.Utilities.HelperFunctions;
import com.spechtuhdelangbein.gamepad_phone.R;
import com.spechtuhdelangbein.gamepad_phone.Utilities.XMLParser;

import java.io.File;
import java.util.Locale;

/**
 * Created by Constantin on 02.08.2015.
 */
public class Activity_Editor extends Activity {

    private RelativeLayout relLay;
    public RelativeLayout.LayoutParams layParams;

    //Drawer_START=========================
    private String[] drawerEntries;
    private DrawerLayout drawerLayout;
    private ListView drawerList;
    private EditText drawerEdit;
    private LinearLayout drawerLinear;
    //Drawer_END=========================

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);

        setLanguage();

        relLay = (RelativeLayout)findViewById(R.id.editor_relLay);
        String filepath = Configuration_Others.layoutStandard;

        //Drawer_START=========================
        drawerLinear = (LinearLayout) findViewById(R.id.editor_drawer_linear);
        drawerEdit =(EditText) findViewById(R.id.editor_drawer_edit);
        drawerEntries = getResources().getStringArray(R.array.editor_drawer);
        drawerLayout = (DrawerLayout) findViewById(R.id.editor_drawer);
        drawerList = (ListView) findViewById(R.id.editor_drawer_listview);

        drawerList.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,drawerEntries));
        drawerList.setOnItemClickListener(new DrawerItemClickListener());
        //Drawer_END=========================

        //Load elements, currently without Gesture Listeners
        GamepadElements.loadEditorElements(relLay, this, filepath);

        //Iterate through all views and add Gesture Listeners
        addEditorListener();
        relLay.setOnDragListener(new EditorDragListener(relLay));

    }

    @Override
    protected void onResume() {
        setLanguage();
        super.onResume();
    }

    private class EditorOnTouchListener implements View.OnTouchListener{
        @Override
        public boolean onTouch(View v,MotionEvent e){
            if (e.getAction()==MotionEvent.ACTION_DOWN) {
                ClipData data = ClipData.newPlainText("", "");
                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
                v.startDrag(data, shadowBuilder, v, 0);
                v.setVisibility(View.INVISIBLE);
            }
            return true;
        }
    }


    private class EditorScaleGestureListener implements View.OnTouchListener,ScaleGestureDetector.OnScaleGestureListener{
        private View view;
        private ScaleGestureDetector scaleDetector;
        private GestureDetector gestureDetector;
        private float scaleFactor=1;
        public boolean inScale = false;

        public EditorScaleGestureListener(Context context){
            scaleDetector = new ScaleGestureDetector(context,this);
            gestureDetector = new GestureDetector(context, new EditorGestureListener(context));
        }

        @Override
        public boolean onTouch(View v,MotionEvent e){
            boolean retVal = scaleDetector.onTouchEvent(e);
                this.view = v;
                retVal = gestureDetector.onTouchEvent(e)||retVal;

            if(Configuration_Others.editorLongPress ==true){
            ClipData data = ClipData.newPlainText("", "");
            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
            view.startDrag(data, shadowBuilder, view, 0);
            view.setVisibility(View.INVISIBLE);
            Configuration_Others.editorLongPress =false;
            }
            return retVal;
        }

        @Override
        public boolean onScale(ScaleGestureDetector detector){
            scaleFactor *= detector.getScaleFactor();
            scaleFactor = Math.max(0.1f, Math.min(scaleFactor, 5.0f)); //Avoid very small scales
            scaleFactor = ((float)((int)(scaleFactor*100)))/100; // Precision change to reduce jitter
            view.setScaleX(scaleFactor);
            view.setScaleY(scaleFactor);
            return true;
        }


        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector){
            Log.d("ScaleDetector: ","onScaleBegin");
            inScale=true;
            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector detector){
            Log.d("ScaleDetector: ","onScaleEnd");
            inScale=false;
            int oldHeight=view.getLayoutParams().height;
            int oldWidth=view.getLayoutParams().width;
            float oldPosX = view.getX();
            float oldPosY = view.getY();
            float centerX = oldPosX+(oldWidth/2);
            float centerY = oldPosY+(oldHeight/2);
            float scaleX = view.getScaleX();
            float scaleY = view.getScaleY();
            int newWidth =(int)(oldWidth*scaleX);
            int newHeight =(int)(oldHeight*scaleY);
            view.setX(centerX+((oldPosX-centerX)*scaleX));
            view.setY(centerY+((oldPosY-centerY)*scaleY));
            view.setScaleY(1);
            view.setScaleX(1);
            view.setLayoutParams(new RelativeLayout.LayoutParams(newWidth,newHeight));

            scaleFactor=1;

        }
    }

    private class EditorGestureListener extends GestureDetector.SimpleOnGestureListener{
        Context context;

        public EditorGestureListener(Context c){
            this.context=c;
        }

        private static final String DEBUG_TAG = "Gestures";
        @Override
        public void onLongPress(MotionEvent event) {

            Configuration_Others.editorLongPress =true;
            Log.d(DEBUG_TAG, "onDown: " + event.toString());
        }
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener{
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id){
            selectItem(position);
        }

    }
    private void selectItem(int position){
        String layoutName=drawerEdit.getText().toString();
        switch(position){
            case 0:

                File target = new File(Configuration_Others.layoutDirectory+layoutName+".xml");
                if(!layoutName.isEmpty()&&target.exists()) {
                    GamepadElements.loadEditorElements(relLay, this, layoutName + ".xml");
                    addEditorListener();
                    Toast.makeText(this, getResources().getString(R.string.editor_load_msg), Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(this, getResources().getString(R.string.editor_load_fail), Toast.LENGTH_SHORT).show();
                }
                break;
            case 1:
                if(!layoutName.isEmpty()&& HelperFunctions.isExternalStorageWritable()) {
                    XMLParser parser = new XMLParser();
                    GamepadElements.saveElements(relLay, this, layoutName);
                    Toast.makeText(this,getResources().getString(R.string.editor_save_msg), Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(this, getResources().getString(R.string.editor_save_fail), Toast.LENGTH_SHORT).show();
                }
                break;
            case 2:
                    GamepadElements.loadEditorElements(relLay, this, Configuration_Others.layoutStandard);
                    addEditorListener();
                    Toast.makeText(this, getResources().getString(R.string.editor_reset_msg), Toast.LENGTH_SHORT).show();
                break;

        }
        drawerLayout.closeDrawer(drawerLinear);
    }

    private void addEditorListener(){
        //Iterate through all views and add Gesture Listeners
        int childCount = relLay.getChildCount();
        if (childCount>0) {
            for (int i = 0; i < childCount; i++) {
                View v = relLay.getChildAt(i);
                v.setOnTouchListener(new EditorScaleGestureListener(this));
            }
        }
    }

    public void setLanguage() {
        Log.d("Milestone", "Setting language");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String languageKey = "com.spechtuhdelangbein.gamepad_phone.language";
        String language = prefs.getString(languageKey, "NULL");

        if(language.equals("NULL")) {
            Log.d("Milestone", "No language found");
            return;
        }
        else {
            Log.d("Milestone", "Setting language to "+language);
            Locale myLocale = new Locale(language);
            Resources res = getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            conf.locale = myLocale;
            res.updateConfiguration(conf, dm);
        }
    }
}
