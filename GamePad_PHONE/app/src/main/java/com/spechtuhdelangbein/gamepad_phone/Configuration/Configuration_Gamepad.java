package com.spechtuhdelangbein.gamepad_phone.Configuration;

/**
 * Created by benedikt on 21.01.17.
 */

public class Configuration_Gamepad {

    // Positioning of the Buttons
    // For more information look at CommandHandler or the documentation
    public final static int[] DPAD_UP = {1,0};
    public final static int[] DPAD_DOWN = {1,1};
    public final static int[] DPAD_LEFT = {1,2};
    public final static int[] DPAD_RIGHT = {1,3};
    public final static int[] START = {1,4};
    public final static int[] BACK = {1,5};
    public final static int[] LEFT_STICK_PRESS = {1,6};
    public final static int[] RIGHT_STICK_PRESS = {1,7};
    public final static int[] LB = {2,0};
    public final static int[] RB = {2,1};
    public final static int[] XBOX_LOGO = {2,2};
    public final static int[] BUTTON_A = {2,4};
    public final static int[] BUTTON_B = {2,5};
    public final static int[] BUTTON_X = {2,6};
    public final static int[] BUTTON_Y = {2,7};
    public final static int LEFT_STICK_X  = 5;
    public final static int LEFT_STICK_Y  = 7;
    public final static int RIGHT_STICK_X = 9;
    public final static int RIGHT_STICK_Y = 11;
    public final static int LEFT_TRIGGER    = 3;
    public final static int RIGHT_TRIGGER   = 4;

    //Offset if the Axis for Touchpad Input
    public final static int XAXIS = 1;
    public final static int YAXIS = 5;
    public final static int[] leftClick = {9,0};
    public final static int[] rightCLick = {9,1};

    //Gamepad Element IDs
    //Button IDs
    public static final int LOWEST_BUTTON  = 100;
    public static final int ID_DPAD_UP      =100;
    public static final int ID_DPAD_DOWN   = 101;
    public static final int ID_DPAD_LEFT   = 102;
    public static final int ID_DPAD_RIGHT  = 103;
    public static final int ID_START       = 104;
    public static final int ID_BACK        = 105;
    public static final int ID_LEFT_STICK_PRESS    = 106;
    public static final int ID_RIGHT_STICK_PRESS   = 107;
    public static final int ID_LB          = 108;
    public static final int ID_RB          = 109;
    public static final int ID_XBOX_LOGO   = 110;
    public static final int ID_BUTTON_A    = 111;
    public static final int ID_BUTTON_B    = 112;
    public static final int ID_BUTTON_X    = 113;
    public static final int ID_BUTTON_Y    = 114;
    public static final int ID_TOUCHPAD    = 115;
    public static final int HIGHEST_BUTTON  = 115;

    //Analog Stick IDs
    public static final int LOWEST_STICK     = 116;
    public static final int ID_LEFT_STICK  = 116;
    public static final int ID_RIGHT_STICK = 117;
    public static final int HIGHEST_STICK   = 117;

    //Analog Trigger IDs
    public static final int LOWEST_TRIGGER  = 118;
    public static final int ID_LEFT_TRIGGER = 118;
    public static final int ID_RIGHT_TRIGGER = 119;
    public static final int HIGHEST_TRIGGER = 119;

    /*
        Factors needed for Triggers and Sticks. Used to transform the 0 to 1 output range
        to maximum size for CommandHandler
    */
    //Factors
    public static final float TRIGGER_FACTOR = 0.00392156862f;
    public static final float STICK_FACTOR = 0.000030518509475f;
}
