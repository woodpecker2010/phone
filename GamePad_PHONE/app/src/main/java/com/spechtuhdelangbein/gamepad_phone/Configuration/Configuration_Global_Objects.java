package com.spechtuhdelangbein.gamepad_phone.Configuration;

import com.spechtuhdelangbein.gamepad_phone.Connection.ConnectedThread;
import com.spechtuhdelangbein.gamepad_phone.Utilities.CommandHandler;
import com.spechtuhdelangbein.gamepad_phone.Utilities.Helper_NotificationClass;

/**
 * List of Objects used all over the area of this project
 * Created by benedikt on 21.01.17.
 */

public class Configuration_Global_Objects {

    // Configuration_Others Objects to Access
    public static ConnectedThread mConnectedThread;
    public static Helper_NotificationClass mhnc;
    public static CommandHandler commandHandler;

}
