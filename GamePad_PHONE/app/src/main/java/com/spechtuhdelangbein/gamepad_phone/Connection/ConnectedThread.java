package com.spechtuhdelangbein.gamepad_phone.Connection;


import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.spechtuhdelangbein.gamepad_phone.Configuration.Configuration_Global_Objects;
import com.spechtuhdelangbein.gamepad_phone.Configuration.Configuration_Others;
import com.spechtuhdelangbein.gamepad_phone.Debug.CommandWatcher;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


/**
 * Created by Andreas on 08.06.2015.
 */
public class ConnectedThread extends Thread {

    final int BYTE_ARRAY_LENGTH = Configuration_Others.BYTE_ARRAY_LENGTH;

    private final BluetoothSocket m_BTSocket;
    private final InputStream m_IncomingStream;
    private final OutputStream m_OutgoingStream;
    private final CommandWatcher commandWatcher;
    ConnectionLobby mConLobby;
    private Context context;


    public boolean connected = false;

    public ConnectedThread(BluetoothSocket socket,ConnectionLobby cl) {
        m_BTSocket = socket;
        InputStream tmpIn = null;
        OutputStream tmpOut = null;
        mConLobby = cl;


        try {
            tmpIn = socket.getInputStream();
            tmpOut = socket.getOutputStream();
        } catch (IOException e) { }

        m_IncomingStream = tmpIn;
        m_OutgoingStream = tmpOut;
        commandWatcher = new CommandWatcher();
    }

    public void run() {
        byte[] buffer = new byte[BYTE_ARRAY_LENGTH];
        int bytes; // amount of bytes we received
        //mConLobby.showToast("Good job! Your devices are connected");
        connected = true;

        while (connected) {
            /**************************************************************************************
            ***** If this is activated the method receiveNewPlayerNumber doesn't work anymore *****
            try {
                // Try to read all incoming data
                bytes = m_IncomingStream.read(buffer);
                //Send incoming data to sink-Method
                IncomingDataSink(buffer,bytes);

            } catch (IOException e) {
                Log.d("Milestone","Cannot read. Connection closes");
                connected = false;
                cancel();
                break;
            }
            ***************************************************************************************/
        }
    }

    /** Call this from the main activity to send data to the remote device */
    public boolean write(byte[] bytes) {
        if (commandWatcher.isPackageCorrupt(bytes))
            Log.e("CommandWatcher","Currently sent Data package is corrupt!");
        try {
            m_OutgoingStream.write(bytes);
            //connected = true;
            return true;
        } catch (IOException e)
        {
            Log.d("Milestone","But cannot write. Connection is closed");
            connected = false;
            cancel();
            return false;
        }
    }

    /** Call this from the main activity to shutdown the connection */
    public void cancel() {
        try {
            m_BTSocket.close();
            connected = false;
            mConLobby.Disconnected(this);
            Configuration_Global_Objects.mhnc.Notify(mConLobby, "Lost connection to tablet. Please re-establish", "Sorry", 1);
            this.interrupt();
        } catch (IOException e)
        {
            Log.d("Milestone", "Could not close socket");
        }
    }

    public void IncomingDataSink(byte[] input, int byteCount)
    {
        String ReceivedData = "";

        for(int i = 0; i < byteCount; i++)
        {
            ReceivedData += input[i];
        }
    }

    /**
     * Tells whether the thread currently is connected.
     * @return true if connected, false if not connected
     */
    public boolean isConnected(){
        return connected;
    }

    public BluetoothSocket GetSocket()
    {
        return m_BTSocket;
    }

    /**
     * Sends a request to the connected Tablet for a new Player No.
     * Fetches up to 1000 times the information from the {@link ConnectedThread#m_IncomingStream}
     * until it contains information about the defined player number for this device
     * @return -1 if the process fails, else the player number
     */
    public int receiveNewPlayerNo(){
        try{
            Thread.sleep(300);
        } catch (InterruptedException e) {}
        int playerNo;
        byte[] buffer = new byte[BYTE_ARRAY_LENGTH];

        // Send the request to the Tablet
        Log.i("ConnectedThread", "Asking for a player ID");
        buffer[0] = Configuration_Others.TYPE_CONNECTION_REQUEST;
        try {
            m_OutgoingStream.write(buffer);
        } catch (IOException e) {
            Log.e("ConnectedThread", "Error while sending the request");
        }


        // Receive the answer from the Tablet
        // if something unexpected happens the Tablet will send a QUIT message
        // in this case we want to return to the main menu to be able try to connect again
        try {
            int counter = 0;
            while (counter < 100) {
                counter ++;
                m_IncomingStream.read(buffer, 0, BYTE_ARRAY_LENGTH);
                if ((buffer[0] & 0x000F) == Configuration_Others.TYPE_INFORMATION) {
                    playerNo = buffer[0] >> 4;
                    Log.i("ConnectedThread", "Received following PlayerID: " + playerNo);
                    return playerNo;
                } else
                if ((buffer[0] & 0x000F) == Configuration_Others.TYPE_QUIT_CONNECTION) {
                    if (context != null) {
                        Intent goToLobby = new Intent(context,ConnectionLobby.class);
                        context.startActivity(goToLobby);
                    }
                    return -1;
                }

            }
        } catch (IOException e) {
            Log.e("ConnectedThread","Error while receiving player number");
        }
        return -1;
    }

    /**
     * Simple setter method
     * @param c set the current activity's context
     */
    public void setContext(Context c){
        context = c;
    }




}

