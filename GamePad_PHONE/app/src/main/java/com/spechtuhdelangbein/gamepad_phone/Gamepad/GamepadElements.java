package com.spechtuhdelangbein.gamepad_phone.Gamepad;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.spechtuhdelangbein.gamepad_phone.Configuration.Configuration_Gamepad;
import com.spechtuhdelangbein.gamepad_phone.Configuration.Configuration_Others;
import com.spechtuhdelangbein.gamepad_phone.Utilities.CommandHandler;
import com.spechtuhdelangbein.gamepad_phone.Utilities.HelperFunctions;
import com.spechtuhdelangbein.gamepad_phone.Utilities.XMLParser;
import com.spechtuhdelangbein.gamepad_phone.Utilities.XmlElement;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Constantin on 19.07.2015.
 */
public class GamepadElements {

    /**
     * This function is called in Activity_Editor
     * Creates instances of all EditorGamepadElements defined in the xml at filepath
     * @param relLay The relative layout which will hold the instances
     * @param context takes the handle to the main activity context (i.e. "this")
     * @param filename represents the filepath relative to external storage of the layout xml file
     */
    public static void loadEditorElements(RelativeLayout relLay,Context context,String filename){

        //remove all views prior to populating the layout with gamepad elements
        relLay.removeAllViews();

        List<XmlElement> elementList = XMLParser.readXmlFromFile(Configuration_Others.layoutDirectory+filename);
        for (XmlElement element :elementList){
            if (element.getID()>= Configuration_Gamepad.LOWEST_BUTTON && element.getID()<= Configuration_Gamepad.HIGHEST_BUTTON){
                int buttonID = element.getID();
                int posX = HelperFunctions.dpToPixels(context, element.getX());
                int posY = HelperFunctions.dpToPixels(context,element.getY());
                int size = HelperFunctions.dpToPixels(context,element.getSize());
                GamepadElements.editorButton(relLay, context, buttonID, posX, posY, size);
            } else if(element.getID()>= Configuration_Gamepad.LOWEST_STICK && element.getID()<= Configuration_Gamepad.HIGHEST_STICK){
                int stickID = element.getID();
                int posX = HelperFunctions.dpToPixels(context,element.getX());
                int posY = HelperFunctions.dpToPixels(context,element.getY());
                int size = HelperFunctions.dpToPixels(context,element.getSize());
                GamepadElements.editorAnalogStick(relLay, context, stickID, posX, posY, size);
            } else if(element.getID()>= Configuration_Gamepad.LOWEST_TRIGGER && element.getID()<= Configuration_Gamepad.HIGHEST_TRIGGER){
                int triggerID = element.getID();
                int posX = HelperFunctions.dpToPixels(context,element.getX());
                int posY = HelperFunctions.dpToPixels(context,element.getY());
                int size = HelperFunctions.dpToPixels(context,element.getSize());
                GamepadElements.editorTrigger(relLay, context, triggerID, posX, posY, size);
            }
        }
    }
    /**
     * This function is called in Activity_Gamepad
     * Creates instances of all GamepadElements defined in the xml at filepath
     * @param relLay The relative layout which will hold the instances
     * @param context takes the handle to the main activity context (i.e. "this")
     * @param touchListener takes the reference to the GamepadListener, which handles button touch events
     * @param filename represents the filepath relative to external storage of the layout xml file
     * @param commandHandler takes the reference to the CommandHandler which is responsible for sending the input commands
     */
    public static void loadElements(RelativeLayout relLay,Context context,View.OnTouchListener touchListener,String filename,CommandHandler commandHandler){
        List<XmlElement> elementList = XMLParser.readXmlFromFile(Configuration_Others.layoutDirectory+filename);
        //Create buttons and analog controls based on elements in list
        for (XmlElement element :elementList){
            if (element.getID()>= Configuration_Gamepad.LOWEST_BUTTON && element.getID()<= Configuration_Gamepad.HIGHEST_BUTTON){
                int buttonID = element.getID();
                int posX = HelperFunctions.dpToPixels(context,element.getX());
                int posY = HelperFunctions.dpToPixels(context,element.getY());
                int size = HelperFunctions.dpToPixels(context,element.getSize());
                GamepadElements.createButton(relLay, context, touchListener,buttonID,posX,posY,size);
            } else if(element.getID()>= Configuration_Gamepad.LOWEST_STICK && element.getID()<= Configuration_Gamepad.HIGHEST_STICK){
                int stickID = element.getID();
                int posX = HelperFunctions.dpToPixels(context,element.getX());
                int posY = HelperFunctions.dpToPixels(context,element.getY());
                int size = HelperFunctions.dpToPixels(context,element.getSize());
                GamepadElements.createAnalogStick(relLay,context,stickID,posX,posY,size,commandHandler);
            } else if(element.getID()>= Configuration_Gamepad.LOWEST_TRIGGER && element.getID()<= Configuration_Gamepad.HIGHEST_TRIGGER){
                int triggerID = element.getID();
                int posX = HelperFunctions.dpToPixels(context,element.getX());
                int posY = HelperFunctions.dpToPixels(context,element.getY());
                int size = HelperFunctions.dpToPixels(context,element.getSize());
                GamepadElements.createTrigger(relLay, context, triggerID,posX,posY,size, commandHandler);
            }
        }
    }



    public static boolean saveElements(RelativeLayout relLAy,Context context,String filename){
        int childCount = relLAy.getChildCount();
        List<XmlElement> elementList = new ArrayList<XmlElement>();

        if (childCount>0) {
            for (int i = 0; i < childCount; i++) {
                View v = relLAy.getChildAt(i);
                XmlElement element = new XmlElement();
                element.setID(v.getId());
                element.setX(HelperFunctions.pixelsToDp(context, (int) v.getX()));
                element.setY(HelperFunctions.pixelsToDp(context, (int) v.getY()));
                element.setSize(HelperFunctions.pixelsToDp(context, v.getLayoutParams().height));
                elementList.add(element);
            }

            XMLParser.writeXmlToFile(Configuration_Others.layoutDirectory + filename + ".xml", elementList);

            //Make screenshot and save to same directory
            Bitmap image = HelperFunctions.getBitmap(relLAy,relLAy.getWidth(),relLAy.getHeight());
            HelperFunctions.saveThumbnail(image,filename);

            return true;
        } else{
            return false;
        }
    }

    /**
     * Creates a button in the main relative layout of Activity_Gamepad
     * @param rl takes the handle for the relative layout where the button is created
     * @param context takes the handle to the main activity context (i.e. "this")
     * @param buttonID specifies which button type will be created
     * @param posX specifies the X position in the layout in px
     * @param posY specifies the Y position in the layout in px
     * @param size specifies the diameter of the button in dp
     * @return Returns an Byte array with 2 fields. The Number can be read as position 0 is at byte[1].
     */

    public static ImageButton editorButton(RelativeLayout rl,Context context, int buttonID, int posX,int posY,int size){



        ImageButton button= new ImageButton(context);

        if(buttonID== Configuration_Gamepad.ID_LB||buttonID== Configuration_Gamepad.ID_RB){
            button.setLayoutParams(new RelativeLayout.LayoutParams(size, size/2));
        }else {
            button.setLayoutParams(new RelativeLayout.LayoutParams(size, size));
        }
        button.setId(buttonID);
        button.setX(posX);
        button.setY(posY);
        //Sets Image ID based on assigned ID-key in Configuration_Others.imageMap
        button.setImageResource(Configuration_Others.imageMap.get(buttonID));
        button.setBackgroundColor(Color.TRANSPARENT);
        button.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        rl.addView(button);

    return button;
    }


    public static void createButton(RelativeLayout rl,Context context,View.OnTouchListener listener, int buttonID, int posX,int posY,int size){



        ImageButton button= new ImageButton(context);
        if(buttonID== Configuration_Gamepad.ID_LB||buttonID== Configuration_Gamepad.ID_RB){
           button.setLayoutParams(new RelativeLayout.LayoutParams(size, size/2));
        }else {
            button.setLayoutParams(new RelativeLayout.LayoutParams(size, size));
       }
        button.setId(buttonID);
        button.setX(posX);
        button.setY(posY);
        //Sets Image ID based on assigned ID-key in Configuration_Others.imageMap
        button.setImageResource(Configuration_Others.imageMap.get(buttonID));
        button.setBackgroundColor(Color.TRANSPARENT);
        button.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

            button.setOnTouchListener(listener);
            rl.addView(button);

    }

    public static void editorAnalogStick(RelativeLayout rl, Context context, int stickID, int posX, int posY,int size){
        GamepadAnalogSticks stick = new GamepadAnalogSticks(context,stickID);

        stick.setLayoutParams(new RelativeLayout.LayoutParams(size, size));
        stick.setX(posX);
        stick.setY(posY);
        stick.setId(stickID);

        rl.addView(stick);
    }
    public static void createAnalogStick(RelativeLayout rl, Context context, int stickID, int posX, int posY,int size,CommandHandler commandHandler){
        GamepadAnalogSticks stick = new GamepadAnalogSticks(context,commandHandler,stickID);

        stick.setLayoutParams(new RelativeLayout.LayoutParams(size, size));
        stick.setX(posX);
        stick.setY(posY);
        stick.setId(stickID);


        rl.addView(stick);
        }

    public static void editorTrigger(RelativeLayout rl, Context context, int triggerID,int posX, int posY, int size){
        GamepadTriggers trigger = new GamepadTriggers(context,triggerID);

        int width   = size/2;
        int height  = size;

        trigger.setLayoutParams(new RelativeLayout.LayoutParams(width, height));
        trigger.setX(posX);
        trigger.setY(posY);
        trigger.setId(triggerID);

        rl.addView(trigger);
    }

    public static void createTrigger(RelativeLayout rl, Context context, int triggerID, int posX, int posY,int size,CommandHandler commandHandler){
        GamepadTriggers trigger = new GamepadTriggers(context,commandHandler,triggerID);

        int width   = size/2;
        int height  = size;

        trigger.setLayoutParams(new RelativeLayout.LayoutParams(width, height));
        trigger.setX(posX);
        trigger.setY(posY);
        trigger.setId(triggerID);

        rl.addView(trigger);
    }
    }
