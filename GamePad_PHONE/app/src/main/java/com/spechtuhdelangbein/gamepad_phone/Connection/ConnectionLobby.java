package com.spechtuhdelangbein.gamepad_phone.Connection;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.spechtuhdelangbein.gamepad_phone.Activities.Activity_Gamepad;
import com.spechtuhdelangbein.gamepad_phone.Configuration.Configuration_Global_Objects;
import com.spechtuhdelangbein.gamepad_phone.Utilities.Helper_NotificationClass;
import com.spechtuhdelangbein.gamepad_phone.R;

import java.util.Set;


public class ConnectionLobby extends Activity {

    //Contains all scanned devices
    private ArrayAdapter<String> BTArrayAdapter_UnknownDevs;
    //Contains all pre-paired devices
    private ArrayAdapter<String> BTArrayAdapter_ExPairedDevs;

    Helper_NotificationClass hnc;

    Button mbtn_Scan;
    ListView mDiscoveredDevsList;
    ListView mFormerPairedDevsList;
    BluetoothAdapter mBluetoothAdapter;

    /** Is assigned by another method. Once its not null we can use it to send data */
    public ConnectedThread connectedT;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection_lobby);

        mbtn_Scan = (Button) findViewById(R.id.btn_Scan);


        mDiscoveredDevsList = (ListView) findViewById(R.id.DeviceList);
        mFormerPairedDevsList = (ListView) findViewById(R.id.KnownDeviceList);

        if(Configuration_Global_Objects.mhnc == null)
        {
           hnc = new Helper_NotificationClass();
            Configuration_Global_Objects.mhnc = hnc;
        }
        else
        {
            hnc = Configuration_Global_Objects.mhnc;
        }


        //We instantiate the List-Adapters showing all device results
        BTArrayAdapter_UnknownDevs = new ArrayAdapter<String>(this,R.layout.mycenteredlistitem);
        ((ListView)findViewById(R.id.DeviceList)).setAdapter(BTArrayAdapter_UnknownDevs);

        BTArrayAdapter_ExPairedDevs = new ArrayAdapter<String>(this,R.layout.mycenteredlistitem);
        ((ListView)findViewById(R.id.KnownDeviceList)).setAdapter(BTArrayAdapter_ExPairedDevs);


        //Check if the device supports bluetooth at all
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
                hnc.ShowAlertDialog(this);
        }
        else
        {
            //Checks for enabled bluetooth and prompts the user to activate it
            EnableBluetooth();
            // Here we register the Receiver to specify how we react to devices in the area
            IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            //Gets unregistered during OnDestroy
            registerReceiver(bReceiver, filter);
            //Generates and shows the list of devices we've been connected to before
            CheckFormerPairedDevices();
        }

        mbtn_Scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                find(v);
            }
        });

        //The Main List contains all found bluetooth devices that are currently discoverable
        mDiscoveredDevsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Connect(position, "Unknown");

            }
        });
        //The second List contains all former paired devices that we already know
        mFormerPairedDevsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Connect(position,"Paired");

            }
        });

        //Start searching for devices right away
        find(null);

    }

    /** Every time we return to this screen we have to reload the former paired devices */
    @Override
    protected void onResume() {
        super.onResume();
        if (BluetoothAdapter.getDefaultAdapter() != null)
        {
            CheckFormerPairedDevices();
        }
    }

    /** Checks whether or not Bluetooth is enabled and asks the user to allow connecting */
    public void EnableBluetooth()
    {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(!mBluetoothAdapter.isEnabled())
        {
            Intent enabBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivity(enabBT);
        }
    }


    /**
    * The broadcast receiver specifies how we react to finding a nearby bluetooth device
    * */
    final BroadcastReceiver bReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                if(device.getName()!=null)
                {
                    // add the name and the MAC address of the object to the arrayAdapter
                    BTArrayAdapter_UnknownDevs.add(device.getName() + "\n" + device.getAddress());
                    BTArrayAdapter_UnknownDevs.notifyDataSetChanged();
                }

            }
        }
    };


    public void find(View view)
    {
        BTArrayAdapter_UnknownDevs.clear();
        BluetoothAdapter.getDefaultAdapter().startDiscovery();
    }

    /**
    *   Receives a ListPosition und a String showing whether we use the list of new devices
    *   or the list of former paired devices
    *   In both cases we take the adress and start the connecting thread
    * **/
    public void Connect(int ItemPosition,String UnknownOrPaired)
    {

        String item = "";
        if(UnknownOrPaired.equals("Unknown")) {
            item = BTArrayAdapter_UnknownDevs.getItem(ItemPosition);
        }
        else
        {
            item = BTArrayAdapter_ExPairedDevs.getItem(ItemPosition);
        }
        String[] itemAndAdress = item.split("\\r?\\n");

        BluetoothDevice Rem = mBluetoothAdapter.getRemoteDevice(itemAndAdress[1]);

        ConnectThread ct = new ConnectThread(mBluetoothAdapter.getRemoteDevice(itemAndAdress[1]),mBluetoothAdapter,this);
        ct.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(isFinishing())
        {
            if(Configuration_Global_Objects.mConnectedThread!=null) {
                Configuration_Global_Objects.mConnectedThread.cancel();
            }
        }
    }

    /** Disables Bluetooth and unregisters the device-receiver when the app is destroyed */
    @Override
    protected void onDestroy() {

        super.onDestroy();

        if(isFinishing())
        {
            if(Configuration_Global_Objects.mConnectedThread!=null) {
                Configuration_Global_Objects.mConnectedThread.cancel();
            }
            unregisterReceiver(bReceiver);
        }
    }


    /**
     * Called from the ConnectThread when the connection was established successfully
     * Once the connectedT is assigned we can use it to send write and read commands
     */
    public void Connected(ConnectedThread ct)
    {
        connectedT = ct;
        Configuration_Global_Objects.mConnectedThread = ct;
        //Here we can start the next activity passing the connected Thread
        //startActivity(startGamePadActivity);
        Intent startGamePadActivity = new Intent(this,Activity_Gamepad.class);
        startActivity(startGamePadActivity);
    }

    public void Disconnected(ConnectedThread ct)
    {
    }


    /**
      * Clears the List with all devices that we've been paired to before
      * AND finds all former paired devices a new.
     */
    public void CheckFormerPairedDevices()
    {
        BTArrayAdapter_ExPairedDevs.clear();
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        // If there are paired devices
        if (pairedDevices.size() > 0) {
            // Loop through paired devices
            for (BluetoothDevice device : pairedDevices) {
                // Add the name and address to an array adapter to show in a ListView
                BTArrayAdapter_ExPairedDevs.add(device.getName() + "\n" + device.getAddress());
            }
        }
    }








}
