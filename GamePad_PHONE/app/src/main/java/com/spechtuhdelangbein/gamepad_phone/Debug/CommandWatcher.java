package com.spechtuhdelangbein.gamepad_phone.Debug;

import com.spechtuhdelangbein.gamepad_phone.Configuration.Configuration_Others;

/**
 * This class simply checks the first byte of a given byte array
 * on unexpected Values.
 * Created by Benedikt on 11.08.2015.
 */

public class CommandWatcher {
	
	int amount_types;
	int amount_players;
	
	public CommandWatcher() {
		amount_types = Configuration_Others.AMOUNT_TYPES;
		amount_players = 4;
	}
	
	/**
	*	Checks the first byte of the given byte array on realistic numbers.
	*	It validates that the informations are well within the possible ranges
	*	It does not check whether the correct player or types is set.
	*	This package does not guarantee that the given array is well set.
	*	@params array byte array which shall be validated
	*/
	public boolean isPackageCorrupt(byte[] array){
		int type 	= array[0] & 0x0F;
		int player 	= array[0] >> 4;
		if (type < 0 || type >= amount_types)
			return true;
		if (player < 0 || player >= amount_players)
			return true;
		return false;
	}

    /**
     * Checks the first byte of an given byte array on the correct values.
     * It compares the first 4Bit with the value of the second argument (in_type) and
     * the second 4Bits with the third agrument (in_player)
     * @param array The byte array which shall be validated
     * @param in_type the message type. this value will be compared to the first 4Bits of the array
     * @param in_player the player number. This value will be compared to the second 4Bit of the array
     * @return true if everything's allright, false if one if the inputs doesn't match
     */
    public boolean areValuesCorrect (byte[] array, int in_type, int in_player) {
        int type    = array[0] & 0x0F;
        int player  = array[0] >> 4;

        if(type != in_type || player != in_player)
            return false;
        return true;
    }
	
}