package com.spechtuhdelangbein.gamepad_phone.Utilities;

import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.widget.RelativeLayout;

/**
 * Created by Constantin on 03.08.2015.
 */
public class EditorDragListener implements View.OnDragListener{

        int posX;
        int posY;
        RelativeLayout layout;

        public EditorDragListener(RelativeLayout l){
            layout = l;
        }

        @Override
        public boolean onDrag(View v, DragEvent event) {
            int action = event.getAction();
            View view = (View) event.getLocalState();
            RelativeLayout l = (RelativeLayout)view.getParent();
            if(!l.equals(layout))
                return false;
            if(action == DragEvent.ACTION_DRAG_LOCATION){
                posX = (int)event.getX();
                posY = (int)event.getY();
                Log.d("DRAG: ", posX + "," + posY);
            }
            if(action == DragEvent.ACTION_DRAG_ENDED){
                Log.d("DRAG: ", "Dropped at " + posX + "," + posY);
                view.setX(posX-(view.getWidth()/2));
                view.setY(posY-(view.getHeight()/2));
                view.setVisibility(View.VISIBLE);
            }
            return true;
        }
    }
