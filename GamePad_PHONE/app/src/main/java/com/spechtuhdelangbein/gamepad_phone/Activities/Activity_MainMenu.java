package com.spechtuhdelangbein.gamepad_phone.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.spechtuhdelangbein.gamepad_phone.Connection.ConnectionLobby;
import com.spechtuhdelangbein.gamepad_phone.Utilities.HelperFunctions;
import com.spechtuhdelangbein.gamepad_phone.R;

import java.util.Locale;

/**
 * This Activity is the main menu of the App. It simply shows 4 TextViews which lead through a click
 * to the different components.
 * This page automatically sets up it's language.
 *
 * Created by Benedikt on 26.07.2015.
 */
public class Activity_MainMenu extends ActionBarActivity {

    TextView connect;
    TextView editor;
    TextView layouts;
    TextView options;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setLanguage();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);


        //Create Layout directory if not existent
        HelperFunctions.checkLayoutDirectory(this);


        //initializing TextViews
        connect  = (TextView) findViewById(R.id.button_connect);
        editor   = (TextView) findViewById(R.id.button_editor);
        layouts  = (TextView) findViewById(R.id.button_layouts);
        options  = (TextView) findViewById(R.id.button_options);

        // set up the textSize of the TextViews
        // that they appear in the same size on every screen
        float density = getResources().getDisplayMetrics().density;
        connect.setTextSize(16 * density);
        editor.setTextSize(16 * density);
        layouts.setTextSize(16 * density);
        options.setTextSize(16 * density);
    }

    @Override
    protected void onResume() {
        setLanguage();
        super.onResume();
    }

    public void startConnectionLobby (View view){
        startActivity(ConnectionLobby.class);
    }
    public void startEditorActivity (View view) { startActivity(Activity_Editor.class);}
    public void startLayoutActivity (View view) {
        //ToDo
        //Intent activity = new Intent(this, Activity_TouchpadKeyboard.class);
        Intent activity = new Intent(this, Activity_Layout.class);
        startActivity(activity);
        //Toast.makeText(this, "�_�", Toast.LENGTH_SHORT).show();
    }
    public void startOptionsActivity (View view) {
        startActivity(Activity_Options.class);
    }

    /**
     * This method is for starting an Activity through an Intent
     *
     * @param activityClass .class file of the Activity which shall be started
     */
    private void startActivity(Class activityClass){
        Intent activity  = new Intent(this, activityClass);
        startActivity(activity);
    }

    /**
     * Checks whether or not a specific language was pre-set.
     * If so, the displayed text will be translated into the given one.
     * Otherwise the default one will be used
     *
     * This method will be also called from the optionsMenu
     * */
    public void setLanguage() {
        Log.d("Milestone", "Setting language");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String languageKey = "com.spechtuhdelangbein.gamepad_phone.language";
        String language = prefs.getString(languageKey,"NULL");

        if(language.equals("NULL")) {
            Log.d("Milestone", "No language found");
            return;
        }
        else {
            Log.d("Milestone", "Setting language to "+language);
            Locale myLocale = new Locale(language);
            Resources res = getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            conf.locale = myLocale;
            res.updateConfiguration(conf, dm);
        }
    }
}
