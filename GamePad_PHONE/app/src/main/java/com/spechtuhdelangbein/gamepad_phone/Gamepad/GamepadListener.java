package com.spechtuhdelangbein.gamepad_phone.Gamepad;

import android.content.Context;
import android.content.Intent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;

import com.spechtuhdelangbein.gamepad_phone.Configuration.Configuration_Gamepad;
import com.spechtuhdelangbein.gamepad_phone.Utilities.CommandHandler;
import com.spechtuhdelangbein.gamepad_phone.R;
import com.spechtuhdelangbein.gamepad_phone.Activities.Activity_TouchpadKeyboard;

/**
 * Created by Constantin on 20.07.2015.
 */
public class GamepadListener {

    public static View.OnTouchListener createTouchListener(final Context context,final CommandHandler cmdHandler){
        View.OnTouchListener listener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                ImageButton button;

                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    switch (v.getId()) {
                        case Configuration_Gamepad.ID_DPAD_UP:
                            cmdHandler.setBit(Configuration_Gamepad.DPAD_UP);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.dpad_up_button_pressed);
                            break;
                        case Configuration_Gamepad.ID_DPAD_DOWN:
                            cmdHandler.setBit(Configuration_Gamepad.DPAD_DOWN);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.dpad_down_button_pressed);
                            break;
                        case Configuration_Gamepad.ID_DPAD_LEFT:
                            cmdHandler.setBit(Configuration_Gamepad.DPAD_LEFT);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.dpad_left_button_pressed);
                            break;
                        case Configuration_Gamepad.ID_DPAD_RIGHT:
                            cmdHandler.setBit(Configuration_Gamepad.DPAD_RIGHT);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.dpad_right_button_pressed);
                            break;
                        case Configuration_Gamepad.ID_START:
                            cmdHandler.setBit(Configuration_Gamepad.START);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.start_button_pressed);
                            break;
                        case Configuration_Gamepad.ID_BACK:
                            cmdHandler.setBit(Configuration_Gamepad.BACK);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.back_button_pressed);
                            break;
                        case Configuration_Gamepad.ID_LEFT_STICK_PRESS:
                            cmdHandler.setBit(Configuration_Gamepad.LEFT_STICK_PRESS);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.stick_button_pressed);
                            break;
                        case Configuration_Gamepad.ID_RIGHT_STICK_PRESS:
                            cmdHandler.setBit(Configuration_Gamepad.RIGHT_STICK_PRESS);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.stick_button_pressed);
                            break;
                        case Configuration_Gamepad.ID_LB:
                            cmdHandler.setBit(Configuration_Gamepad.LB);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.lb_button_pressed);
                            break;
                        case Configuration_Gamepad.ID_RB:
                            cmdHandler.setBit(Configuration_Gamepad.RB);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.rb_button_pressed);
                            break;
                        case Configuration_Gamepad.ID_XBOX_LOGO:
                            cmdHandler.setBit(Configuration_Gamepad.XBOX_LOGO);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.xbox_button_pressed);
                            break;
                        case Configuration_Gamepad.ID_BUTTON_A:
                            cmdHandler.setBit(Configuration_Gamepad.BUTTON_A);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.a_button_pressed);
                            break;
                        case Configuration_Gamepad.ID_BUTTON_B:
                            cmdHandler.setBit(Configuration_Gamepad.BUTTON_B);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.b_button_pressed);
                            break;
                        case Configuration_Gamepad.ID_BUTTON_X:
                            cmdHandler.setBit(Configuration_Gamepad.BUTTON_X);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.x_button_pressed);
                            break;
                        case Configuration_Gamepad.ID_BUTTON_Y:
                            cmdHandler.setBit(Configuration_Gamepad.BUTTON_Y);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.y_button_pressed);
                            break;
                        case Configuration_Gamepad.ID_TOUCHPAD:
                            Intent activity = new Intent(context, Activity_TouchpadKeyboard.class);
                            context.startActivity(activity);
                            break;
                        default:
                            break;
                    }
                }
                else if (event.getAction() == MotionEvent.ACTION_UP){
                    switch (v.getId()) {
                        case Configuration_Gamepad.ID_DPAD_UP:
                            cmdHandler.clearBit(Configuration_Gamepad.DPAD_UP);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.dpad_up_button);
                            break;
                        case Configuration_Gamepad.ID_DPAD_DOWN:
                            cmdHandler.clearBit(Configuration_Gamepad.DPAD_DOWN);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.dpad_down_button);
                            break;
                        case Configuration_Gamepad.ID_DPAD_LEFT:
                            cmdHandler.clearBit(Configuration_Gamepad.DPAD_LEFT);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.dpad_left_button);
                            break;
                        case Configuration_Gamepad.ID_DPAD_RIGHT:
                            cmdHandler.clearBit(Configuration_Gamepad.DPAD_RIGHT);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.dpad_right_button);
                            break;
                        case Configuration_Gamepad.ID_START:
                            cmdHandler.clearBit(Configuration_Gamepad.START);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.start_button);
                            break;
                        case Configuration_Gamepad.ID_BACK:
                            cmdHandler.clearBit(Configuration_Gamepad.BACK);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.back_button);
                            break;
                        case Configuration_Gamepad.ID_LEFT_STICK_PRESS:
                            cmdHandler.clearBit(Configuration_Gamepad.LEFT_STICK_PRESS);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.stick_button);
                            break;
                        case Configuration_Gamepad.ID_RIGHT_STICK_PRESS:
                            cmdHandler.clearBit(Configuration_Gamepad.RIGHT_STICK_PRESS);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.stick_button);
                            break;
                        case Configuration_Gamepad.ID_LB:
                            cmdHandler.clearBit(Configuration_Gamepad.LB);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.lb_button);
                            break;
                        case Configuration_Gamepad.ID_RB:
                            cmdHandler.clearBit(Configuration_Gamepad.RB);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.rb_button);
                            break;
                        case Configuration_Gamepad.ID_XBOX_LOGO:
                            cmdHandler.clearBit(Configuration_Gamepad.XBOX_LOGO);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.xbox_button);
                            break;
                        case Configuration_Gamepad.ID_BUTTON_A:
                            cmdHandler.clearBit(Configuration_Gamepad.BUTTON_A);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.a_button);
                            break;
                        case Configuration_Gamepad.ID_BUTTON_B:
                            cmdHandler.clearBit(Configuration_Gamepad.BUTTON_B);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.b_button);
                            break;
                        case Configuration_Gamepad.ID_BUTTON_X:
                            cmdHandler.clearBit(Configuration_Gamepad.BUTTON_X);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.x_button);
                            break;
                        case Configuration_Gamepad.ID_BUTTON_Y:
                            cmdHandler.clearBit(Configuration_Gamepad.BUTTON_Y);
                            button=(ImageButton) v;
                            button.setImageResource(R.drawable.y_button);
                            break;
                        default:
                            break;
                    }
                    return true;
                }
                return false;
            }
        };
        return listener;
    }

}
