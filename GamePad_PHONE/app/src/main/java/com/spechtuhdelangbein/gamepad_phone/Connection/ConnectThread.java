package com.spechtuhdelangbein.gamepad_phone.Connection;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import com.spechtuhdelangbein.gamepad_phone.Configuration.Configuration_Global_Objects;

import java.io.IOException;
import java.util.UUID;

/**
 * Created by Andreas on 08.06.2015.
 */
public class ConnectThread extends Thread {
    private final BluetoothSocket m_BTSocket;
    private final BluetoothDevice targetDevice;
    private BluetoothAdapter myBTAdapter;

    private ConnectionLobby mconlobby;

    public ConnectThread(BluetoothDevice device,BluetoothAdapter madap,ConnectionLobby ct) {

        BluetoothSocket tmp = null;

        myBTAdapter = madap;
        mconlobby = ct;
        targetDevice = device;

        try {
            // MY_UUID is the app's UUID string, also used by the server code
            tmp = device.createRfcommSocketToServiceRecord(UUID.fromString("38400000-8cf0-11bd-b23e-10b96e4ef00d"));
        } catch (IOException e)
        {
            Log.d("Connect","Could not connect to tablet");
        }
        m_BTSocket = tmp;
        if(m_BTSocket == null)
        {
            Log.d("Connect","Socket is null");
        }
    }

    public void run() {

        myBTAdapter.cancelDiscovery();

        try {
            //Here we try to connect our devices
            m_BTSocket.connect();
        } catch (IOException connectException) {
            Log.d("Connect","Unable to connect "+connectException.toString());

            //mconlobby.showToast("Sorry,can't connect. Did you enable the connection on your tablet?");
            try {
                m_BTSocket.close();
            } catch (IOException closeException) { }
            return;
        }

        // Start managing data if connected
        manageConnectedSocket(m_BTSocket);
    }

    /** Will cancel an in-progress connection, and close the socket */
    public void cancel() {
        try {
            m_BTSocket.close();
        } catch (IOException e) { }
    }


    public void manageConnectedSocket(BluetoothSocket sock)
    {
        Log.d("Milestone:","We've got a socket on the Phone");
        ConnectedThread cnt = new ConnectedThread(sock,mconlobby);
        cnt.start();
        Configuration_Global_Objects.mhnc.RemoveAllNotifications(mconlobby);
        mconlobby.Connected(cnt);
        this.interrupt();
    }
}
