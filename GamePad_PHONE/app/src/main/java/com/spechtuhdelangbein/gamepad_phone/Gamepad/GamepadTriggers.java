package com.spechtuhdelangbein.gamepad_phone.Gamepad;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.spechtuhdelangbein.gamepad_phone.Configuration.Configuration_Gamepad;
import com.spechtuhdelangbein.gamepad_phone.Configuration.Configuration_Others;
import com.spechtuhdelangbein.gamepad_phone.Utilities.CommandHandler;
import com.spechtuhdelangbein.gamepad_phone.R;


/**
 * Created by Constantin on 22.07.2015.
 * source https://web.archive.org/web/20111229070701/http://www.java2s.com/Open-Source/Android/Widget/mobile-anarchy-widgets/com/MobileAnarchy/Android/Widgets/Joystick/JoystickView.java.htm
 */
public class GamepadTriggers extends View {

    private static final int ID_LEFT_STICK  = 115;
    private static final int ID_RIGHT_STICK = 116;
    private static final int ID_LEFT_TRIGGER    = 117;
    private static final int ID_RIGHT_TRIGGER   = 118;

    private final String Name = "GamepadAnalogStick";
    private Paint basePaint;
    private float posY,postY;
    private AnalogMovedListener listener;
    private CommandHandler cmHandler;
    private int triggerID=ID_LEFT_TRIGGER;
    private boolean editorMode = false;


    //Constructor for Activity_Gamepad, with commandHandler
    public GamepadTriggers(Context context,CommandHandler commandHandler,int id){
        super(context);
        initTriggerView();
        cmHandler=commandHandler;
        triggerID=id;
        setBackgroundResource(Configuration_Others.imageMap.get(id));
    }

    //Constructor for Activity_Editor, without commandHandler
    public GamepadTriggers(Context context,int id){
        super(context);
        initTriggerView();
        editorMode=true;
        triggerID=id;
        setBackgroundResource(Configuration_Others.imageMap.get(id));
    }

    //Initialize AnalogStick
    private void initTriggerView(){
        setFocusable(true);

        basePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        basePaint.setColor(Color.GRAY);
        basePaint.setStrokeWidth(1);
        basePaint.setStyle(Paint.Style.FILL_AND_STROKE);

    }

    //Public methods
    public void setOnTriggerMovedListener(AnalogMovedListener listener){
        this.listener = listener;
    }

    //Drawing


    // correct view size after attached to parent view
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec){

        int measuredWidth = measure(widthMeasureSpec);
        int measuredHeight = measure(heightMeasureSpec);

        int size    = Math.min(measuredWidth,measuredHeight);

        setMeasuredDimension(measuredWidth,measuredHeight);
    }

    private int measure(int measureSpec){
        int result = 0;
        //extract measurement information
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        if(specMode == MeasureSpec.UNSPECIFIED){
            //default value if no bounds are defined
            result=200;
        }else{
            //return full available bounds
            result = specSize;
        }
        return result;
    }

    @Override
    protected void onDraw(Canvas canvas){
        int px = getMeasuredWidth()/2;
        int py = getMeasuredHeight()/2;
        int minDim = Math.min(px,py);

        //draw trigger surface
        //canvas.drawRect(0,0,2*px,2*py,basePaint);



        canvas.save();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        int actionType = event.getAction();
        if(actionType==MotionEvent.ACTION_DOWN){
            if (triggerID == Configuration_Gamepad.ID_LEFT_TRIGGER) {
                this.setBackgroundResource(R.drawable.lt_button_pressed);
            } else {
                this.setBackgroundResource(R.drawable.rt_button_pressed);
            }
        }
        if (actionType == MotionEvent.ACTION_MOVE||actionType==MotionEvent.ACTION_DOWN){
            int px = getMeasuredWidth()/2;
            int py = getMeasuredHeight()/2;

            //get current handle position relative to base
            posY    = (2*py)-event.getY();
            posY    = Math.max(Math.min(posY, 2*py),0);



            postY=posY/(2*py);
            postY=postY/ Configuration_Gamepad.TRIGGER_FACTOR;

            Log.d("Positions", "Y:" + postY);
            //set CommandHandler Variables

            if(!editorMode) {
                if (triggerID == ID_LEFT_TRIGGER) {
                    cmHandler.saveAsByte(Configuration_Gamepad.LEFT_TRIGGER, (int) postY);
                } else {
                    cmHandler.saveAsByte(Configuration_Gamepad.RIGHT_TRIGGER, (int) postY);
                }
            }

            //Force view draw
            invalidate();

            //release stick
        }else if(actionType == MotionEvent.ACTION_UP){

            if (triggerID== Configuration_Gamepad.ID_LEFT_TRIGGER) {
                this.setBackgroundResource(R.drawable.lt_button);
            } else{
                this.setBackgroundResource(R.drawable.rt_button);
            }


            if(!editorMode) {
                //send 0 position to commandHandler
                if (triggerID == ID_LEFT_TRIGGER) {
                    cmHandler.saveStickAxis(Configuration_Gamepad.LEFT_TRIGGER, 0);
                } else if (triggerID == ID_RIGHT_TRIGGER) {
                    cmHandler.saveStickAxis(Configuration_Gamepad.RIGHT_TRIGGER, 0);
                }
            }
        }
        return true;
    }
}
