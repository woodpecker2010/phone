package com.spechtuhdelangbein.gamepad_phone.Activities;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.spechtuhdelangbein.gamepad_phone.Configuration.Configuration_Gamepad;
import com.spechtuhdelangbein.gamepad_phone.Configuration.Configuration_Global_Objects;
import com.spechtuhdelangbein.gamepad_phone.Configuration.Configuration_Others;
import com.spechtuhdelangbein.gamepad_phone.Utilities.CommandHandler;
import com.spechtuhdelangbein.gamepad_phone.R;


/**
 * This Activity displays a Touchpad, as you'll know it from Laptops, on the screen,
 * including two buttons for the mouse presses and one for opening the Keyboard.
 * There is no scrolling supported so far.
 *
 * Depending on whether the Keyboard is opened or not, the Commands send to the Tablet will be
 * either Letters or MouseMovements.
 *
 * Note: The backspace key is only recognized as more than one letter is recognized as typed
 * by the phone itself.
 *
 * Created by Benedikt Specht
 */

public class Activity_TouchpadKeyboard extends ActionBarActivity {

    /** Intervall in miliseconds in which the motion on the touchpad is captured  */
    private final int CAPTURE_DELAY = 5;

    private float startX, startY;
    private float endX, endY;
    private float differenceX, differenceY;
    private int playerNo;

    private Button trackpad;
    private Button leftClick;
    private Button rightClick;
    private CommandHandler commandHandler;
    private TouchPadHandler touchHandler;
    private MotionEvent motion;

    //Keyboard Variables
    private boolean touchpadIsHidden = true;
    private EditText editText;
    private InputMethodManager inputManager;
    private String lastmsg = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_touchpad_keyboard);

        // Initialize current Device Information
        commandHandler  = Configuration_Global_Objects.commandHandler;
        if(commandHandler!=null) {
            playerNo = commandHandler.getPlayerNumber();
            commandHandler.setNewMessageType(Configuration_Others.TYPE_MOUSE, playerNo);
        }


        // Initialize the Mouse Buttons
        // and register a clickListener for each
        leftClick   = (Button) findViewById(R.id.left_click);
        rightClick  = (Button) findViewById(R.id.right_click);
        leftClick. setOnTouchListener(new ClickListener());
        rightClick.setOnTouchListener(new ClickListener());

        // Initialize the Touchpad
        // connect it to a TouchListener to handle presses and releases
        trackpad = (Button) findViewById(R.id.touch_view);
        trackpad.setOnTouchListener(new MyOnTouchListener());


        ///////////////// KEYBOARD THINGS /////////////////////
        inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        editText = (EditText) findViewById(R.id.touchpad_editableText);
        editText.addTextChangedListener(new MyTextWatcher());



    }

    protected void onPause() {
        super.onPause();
        hideKeyboard(this.getCurrentFocus());
        if (commandHandler != null)
            commandHandler.setNewMessageType(Configuration_Others.TYPE_GAMEPAD, playerNo);
    }

    @Override
    protected void onStop(){
        super.onStop();
        hideKeyboard(this.getCurrentFocus());
        if (commandHandler != null)
            commandHandler.setNewMessageType(Configuration_Others.TYPE_GAMEPAD, playerNo);
    }

    @Override
    protected void onResume(){
        super.onResume();
        if(commandHandler!=null)
            commandHandler.setNewMessageType(Configuration_Others.TYPE_MOUSE, playerNo);
    }

    /**
     * Stores the current Touchpad informations into the command handler
     * Make sure that before calling this method the values {@link Activity_TouchpadKeyboard#differenceX}
     * and {@link Activity_TouchpadKeyboard#differenceX} are set correctly
     * @return true after success
     */
    private boolean setTouchCommands() {
        commandHandler.saveTouchpadAxis(differenceX, Configuration_Gamepad.XAXIS);
        commandHandler.saveTouchpadAxis(differenceY, Configuration_Gamepad.YAXIS);
        return true;
    }


    /* **************************************
     *          Keyboard Methods            *
     ****************************************/

    /**
     * Starts the system internal Keyboard to start sending text inputs
     * @param view
     */
    public void showKeyboard(View view){
        touchpadIsHidden = false;
        commandHandler.setNewMessageType(Configuration_Others.TYPE_KEYBOARD, playerNo);
        inputManager.showSoftInput(editText, 0);
        editText.requestFocus();
    }

    /**
     * Hides the Keyboard away
     */
    public void hideKeyboard(View v){
        touchpadIsHidden = true;

        trackpad.requestFocus();
        View view = this.getCurrentFocus();
        if (view != null) {
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }

        commandHandler.setNewMessageType(Configuration_Others.TYPE_MOUSE, playerNo);
    }



    /* **************************************
     *          Inner Classes               *
     ****************************************/


    /**
     * Inner Class for receiving the touch gestures.
     * Has to be outsourced to not block the UI Thread.
     * It simply computes the movement in the x and y direction every 3ms and sends it to the commandHandler
     */
    private class TouchPadHandler extends AsyncTask<MotionEvent, Integer, Long>{
        boolean run = false;

        @Override
        protected Long doInBackground(MotionEvent... event){
            run = true;
            while (run) {
                try {
                    Log.d("Milestone","TouchKey - Doing in background");
                    Thread.sleep(CAPTURE_DELAY);
                    endX = motion.getX();
                    endY = motion.getY();
                    differenceX = startX - endX;
                    differenceY = startY - endY;
                    startX = endX;
                    startY = endY;
                    if(run)
                        setTouchCommands();
                } catch (NullPointerException e) {
                    Log.e("TouchpadHandler", "NullPointer at commandHandler");
                } catch (Exception e) {
                    Log.e("TouchpadHandler", "Exception while receiving Touchpad Input");
                }
            }

            // before stopping reset all informations
            // to clear the sent command array
            differenceX = differenceY = 0;
            setTouchCommands();
            return null;
        }

        public void setRun (boolean b){
            run = b;
        }
    }

    /**
     * A listener for the Mouse Buttons
     * It sets the Bits in the {@link Activity_TouchpadKeyboard#commandHandler}
     * due to the clicked or released Mouse Button
     */
    private class ClickListener implements View.OnTouchListener {



        @Override
        public boolean onTouch(View view, MotionEvent event){
            // in case the keyboard is shown
            // hide it
            Log.d("Milestone","TouchKey - Test Click Listener");
            hideKeyboard(view);

            // check for the source and handle the Action
            if (view.getId() == R.id.left_click)
                return reactToAction(event, Configuration_Gamepad.leftClick);
            else if (view.getId() == R.id.right_click)
                return reactToAction(event, Configuration_Gamepad.rightCLick);
            // if the source didn't fit return false
            return false;
        }

        /**
         * Sets parameters in the commandHandler due to the happened option.
         * Sets the bit if action is {@link MotionEvent#ACTION_DOWN}, or clears it in any other case
         * @param event Event which has to be checked
         * @param button THe corresponding button to the event's source
         */
        public boolean reactToAction(MotionEvent event, int[] button){
            if (event.getAction() == MotionEvent.ACTION_DOWN)
                commandHandler.setBit(button);
            else if (event.getAction() == MotionEvent.ACTION_UP)
                commandHandler.clearBit(button);
            return true;
        }
    }

    /**
     * A Listener for the Trackpad.
     * As soon as there's a touch it starts tracking the motion
     * As soon as there's a release it stops the tracking
     */
    private class MyOnTouchListener implements View.OnTouchListener {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            // in case the keyboard is shown
            // hide it
            if (!touchpadIsHidden)
                hideKeyboard(trackpad);

            // in case the MotionEvent isn't registered yet
            // register it
            if(motion != event)
                motion = event;

            switch (event.getAction()) {
                // Start touch tracking
                case MotionEvent.ACTION_DOWN:
                    startX = event.getX();
                    startY = event.getY();
                    touchHandler = new TouchPadHandler();
                    touchHandler.setRun(true);
                    touchHandler.execute(motion);
                    // End touch tracking
                case MotionEvent.ACTION_UP:
                    touchHandler.setRun(false);
            }
            return true;
        }
    }

    /**
     * Text Watcher for catching and storing the input from the Softkeyboard
     */
    private class MyTextWatcher implements TextWatcher{
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

        @Override
        public void afterTextChanged(Editable editable) {

            System.out.println("TEXT CHANGED");

            if(commandHandler != null) {

                if (editable.toString().length()>0 && editable.toString().subSequence(editable.toString().length() - 1, editable.toString().length()).toString().equalsIgnoreCase("\n")) {
                    //enter pressed
                    System.out.println("ENTER");
                    commandHandler.setChar((char) 0x00013);
                }


                //Backspace
                if(lastmsg.length() > 1) {
                    String tmp = lastmsg.substring(0, lastmsg.length() - 1);

                    // Handle Backspace (UTF-Code: 0x08)
                    if (tmp.equals(editable.toString())) {
                        Log.d("KEYBOARD", "Backspace pressed");
                        System.out.println("BACKSPACE");
                        lastmsg = lastmsg.substring(0, lastmsg.length()-1);
                        commandHandler.setChar((char) 0x0008);
                        return;
                    }
                }

                if(editable.length() > 0) {
                    Log.d("KEYBOARD", "Sending " + editable.charAt(editable.length() - 1));
                    System.out.println("SENDING: "+editable.charAt(editable.length()-1));
                    commandHandler.setChar(editable.charAt(editable.length() - 1));

                    // Handle Space input
                    if (editable.charAt(editable.length() - 1) == ' ') {
                        Log.d("KEYBOARD", "Space pressed");
                        System.out.println("SPACE");
                        commandHandler.setChar(' ');
                    }

                    lastmsg = editable.toString();
                }
            }
        }
    }

}
