package com.spechtuhdelangbein.gamepad_phone.Gamepad;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.spechtuhdelangbein.gamepad_phone.Configuration.Configuration_Gamepad;
import com.spechtuhdelangbein.gamepad_phone.Utilities.CommandHandler;
import com.spechtuhdelangbein.gamepad_phone.R;


/**
 * Created by Constantin on 22.07.2015.
 */
public class GamepadAnalogSticks extends View {


    private final String Name = "GamepadAnalogStick";
    private Paint basePaint, padPaint;
    private float posX, posY,postX,postY;
    private int innerPadding, handleRadius, handleInnerBound, sensitivity;
    private AnalogMovedListener listener;
    private CommandHandler cmHandler;
    private int stickID= Configuration_Gamepad.ID_LEFT_STICK;
    private boolean editorMode = false;
    private Bitmap padBit,baseBit;


    //Constructor for Activity_Gamepad with commandHandler
    public GamepadAnalogSticks(Context context,CommandHandler commandHandler,int id){
        super(context);
        initAnalogView();
        cmHandler=commandHandler;
        stickID=id;

    }

    public GamepadAnalogSticks(Context context,int id){
        super(context);
        initAnalogView();
        stickID=id;
        editorMode = true;
    }

    //Initialize AnalogStick
    private void initAnalogView(){
      setFocusable(true);

        Resources res = getResources();
        padBit = BitmapFactory.decodeResource(res,R.drawable.analog_stick);
        baseBit= BitmapFactory.decodeResource(res,R.drawable.analog_base);
        setBackgroundResource(R.drawable.analog_base);

        //basePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        //basePaint.setColor(Color.GRAY);
        //basePaint.setStrokeWidth(1);
        //basePaint.setStyle(Paint.Style.FILL_AND_STROKE);


        padPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        padPaint.setColor(Color.DKGRAY);
        padPaint.setStrokeWidth(1);
        padPaint.setStyle(Paint.Style.FILL_AND_STROKE);


        innerPadding = 10;
        sensitivity = 10;
    }

    //Public methods
    public void setOnAnalogMovedListener(AnalogMovedListener listener){
        this.listener = listener;
    }

    //Drawing


    // correct view size after attached to parent view
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec){

        int measuredWidth = measure(widthMeasureSpec);
        int measuredHeight = measure(heightMeasureSpec);
        int d = Math.min(measuredWidth, measuredHeight);

        handleRadius = (int) (d*0.25);
        handleInnerBound = handleRadius;

        setMeasuredDimension(d,d);
    }

    private int measure(int measureSpec){
        int result = 0;
        //extract measurement information
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        if(specMode == MeasureSpec.UNSPECIFIED){
            //default value if no bounds are defined
            result=200;
        }else{
            //return full available bounds
            result = specSize;
        }
        return result;
    }

    @Override
    protected void onDraw(Canvas canvas){
        int px = getMeasuredWidth()/2;
        int py = getMeasuredHeight()/2;
        int radius = Math.min(px, py);

        //draw background and pad circle
        //canvas.drawCircle(px,py,radius-innerPadding,basePaint);
        //canvas.drawCircle((int) posX+px, (int) posY+py,handleRadius,padPaint);
        //canvas.drawBitmap(Bitmap.createScaledBitmap(baseBit,px*2,py*2,false),0,0,null);
        canvas.drawBitmap(Bitmap.createScaledBitmap(padBit, handleRadius*2, handleRadius*2, false),(float)posX+px-handleRadius,(float)posY+py-handleRadius,null);

        canvas.save();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        int actionType = event.getAction();
        if (actionType == MotionEvent.ACTION_MOVE){
            int px = getMeasuredWidth()/2;
            int py = getMeasuredHeight()/2;
            int radius = (Math.min(px,py) - handleInnerBound);


            //get current handle position relative to center
            posX    = (event.getX()-px);
            posY    = (event.getY()-py);
            posX    = Math.max(Math.min(posX,radius),-radius);
            posY    = Math.max(Math.min(posY, radius), -radius);
            postX   = posX/(px-(px-radius));
            postY   = posY/(py-(py-radius));
            postX   = postX/ Configuration_Gamepad.STICK_FACTOR;
            postY   = postY/ Configuration_Gamepad.STICK_FACTOR;

            Log.d("PosX ",""+postX);
            Log.d("PosY ",""+postY);



            //set CommandHandler Variables

            if(!editorMode) {
                if (stickID == Configuration_Gamepad.ID_LEFT_STICK) {
                    cmHandler.saveStickAxis(Configuration_Gamepad.LEFT_STICK_X, (int) postX);
                    cmHandler.saveStickAxis(Configuration_Gamepad.LEFT_STICK_Y, (int) postY);
                } else if (stickID == Configuration_Gamepad.ID_RIGHT_STICK) {
                    cmHandler.saveStickAxis(Configuration_Gamepad.RIGHT_STICK_X, (int) postX);
                    cmHandler.saveStickAxis(Configuration_Gamepad.RIGHT_STICK_Y, (int) postY);
                }
            }


            //Pressure
            if(listener != null){
                listener.OnMoved((int)(posX / radius*sensitivity),(int)(posY/radius*sensitivity));
            }
            //Force view draw
            invalidate();

            //release stick
        }else if(actionType == MotionEvent.ACTION_UP){
            returnHandleToCenter();

            //send 0 position to commandHandler
            if(!editorMode) {
                if (stickID == Configuration_Gamepad.ID_LEFT_STICK) {
                    cmHandler.saveStickAxis(Configuration_Gamepad.LEFT_STICK_X, 0);
                    cmHandler.saveStickAxis(Configuration_Gamepad.LEFT_STICK_Y, 0);
                } else if (stickID == Configuration_Gamepad.ID_RIGHT_STICK) {
                    cmHandler.saveStickAxis(Configuration_Gamepad.RIGHT_STICK_X, 0);
                    cmHandler.saveStickAxis(Configuration_Gamepad.RIGHT_STICK_Y, 0);
                }
            }
        }
        return true;
    }

    //returns pad to center position after UP touch event occurred
    private void returnHandleToCenter(){
        Handler handler = new Handler();
        int numberOfFrames = 5;
        final double intervalsX = (0-posX)/numberOfFrames;
        final double intervalsY = (0-posY)/numberOfFrames;

        for (int i=0;i<numberOfFrames;i++){
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    posX += intervalsX;
                    posY += intervalsY;
                    invalidate();
                }
            },i*40);
        }
      if(listener != null){
          listener.OnReleased();
      }
    }

}
