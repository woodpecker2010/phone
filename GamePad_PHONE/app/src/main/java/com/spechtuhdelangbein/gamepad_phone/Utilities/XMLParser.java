package com.spechtuhdelangbein.gamepad_phone.Utilities;

import android.util.Log;
import android.util.Xml;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlSerializer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Constantin on 01.08.2015.
 */
public class XMLParser {


//=========================READING==================================================================


    /**
     * Takes a filepath to a XML File and returns the elements as XmlElement List
     * @param filepath The filepath to the save location
     * @return Returns the XmlElement List
     */

    public static List<XmlElement> readXmlFromFile(String filepath){
        ArrayList<XmlElement> list = new ArrayList<XmlElement>();
        XmlElement element0= new XmlElement(),element1 = new XmlElement();

        XMLParser parser = new XMLParser();

        String xml = parser.getXmlFromFile(filepath);

        Document doc = parser.getDomElement(xml);

        NodeList nl = doc.getElementsByTagName("element");


        for(int i=0;i<nl.getLength();i++){
            Node nd = nl.item(i);
            if (nd.getNodeType() == Node.ELEMENT_NODE) {

                XmlElement element = new XmlElement();

                Element e = (Element) nl.item(i);

                element.setID(Integer.parseInt(parser.getValue(e, "element_id")));
                element.setX(Double.parseDouble(parser.getValue(e, "pos_x")));
                element.setY(Double.parseDouble(parser.getValue(e, "pos_y")));
                element.setSize(Double.parseDouble(parser.getValue(e, "size")));

                list.add(i, element);
            }
        }
        return list;
    }
    /**
     * Tries to retrieve a XML String from the given url address
     * @param url The address to the XML File
     * @return Returns the XML as string
     */
    public static String getXmlFromLUrl(String url){
        String xml = null;

        try{
            //Default Http Client
            DefaultHttpClient httpClient    = new DefaultHttpClient();
            HttpPost httpPost   = new HttpPost(url);

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity   = httpResponse.getEntity();
            xml = EntityUtils.toString(httpEntity);
        } catch(UnsupportedEncodingException e){
            e.printStackTrace();
        } catch(ClientProtocolException e){
            e.printStackTrace();
        } catch(IOException e){
            e.printStackTrace();
        }
        // Return the XML File
        return xml;
    }

    /**
     * Tries to retrieve a XML String from the given filepath
     * @param filepath The path to the XML File as string, relative to external storage
     * @return Returns the XML as string
     */

    public static String getXmlFromFile(String filepath){
        String xml = null;
        try{
            File xmlFile =new File(filepath);
            InputStream fIn = new FileInputStream(xmlFile);

            InputStreamReader inputStreamReader = new InputStreamReader(fIn);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String receiveString = "";
            StringBuilder stringBuilder = new StringBuilder();

            while((receiveString = bufferedReader.readLine())!=null){
                stringBuilder.append(receiveString);
            }
            fIn.close();
            xml=stringBuilder.toString();
            //removes all whitespace from string
            //xml = xml.replaceAll(">\\s+<", "><").trim();
        } catch (FileNotFoundException e) {
            Log.e("Error: ", e.getMessage());
        }catch (IOException e){
            Log.e("Error: ", e.getMessage());
        }
        return xml;
    }

    /**
     *Returns the DOM Element of the given XML File
     * @param xml The XML File in string representation
     * @return Returns the DOM Element as document
     */
    private static Document getDomElement(String xml){
        Document doc   = null;
        DocumentBuilderFactory dbf  = DocumentBuilderFactory.newInstance();
        dbf.setIgnoringElementContentWhitespace(true);
        try{
            DocumentBuilder db = dbf.newDocumentBuilder();

            InputSource is  = new InputSource();
            is.setCharacterStream(new StringReader(xml));
            doc = db.parse(is);
        } catch(ParserConfigurationException e){
            Log.e("Error: ", e.getMessage());
            return null;
        } catch(SAXException e){
            Log.e("Error: ", e.getMessage());
            return null;
        } catch(IOException e){
            Log.e("Error: ", e.getMessage());
            return null;
        }
        // Return the DOM Element as Document
        return doc;
    }

    /**
     * Returns the element value as string
     * @param item The emelent list
     * @param str The name of the Element to analyse
     * @return Returns the Element Value. If no element was found returns empty string
     */
    private  String getValue(Element item,String str){
        NodeList n = item.getElementsByTagName(str);
        return this.getElementValue(n.item(0));
    }
    /**
     * Returns the value of a specific element
     * @param element the Node of the given element
     * @return Returns the element value as string. If no value could be found, returns empty string.
     */
    private final String getElementValue(Node element){
        Node child;
        if(element != null){
            if(element.hasChildNodes()){
                for(child = element.getFirstChild(); child != null; child=child.getNextSibling()){
                    if(child.getNodeType() == Node.TEXT_NODE){
                        //Returns the node value
                        return child.getNodeValue();
                    }
                }
            }
        }
        // Return empty string if no node value is found
        return "";
    }


    //=========================WRITING==============================================================



    public static void writeXmlToFile(String filepath, List<XmlElement> elements){
        saveStringToFile(filepath,writeXmlToString(elements));
    }

    private static String writeXmlToString(List<XmlElement> elements){
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        try{
            serializer.setOutput(writer);
            serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
            serializer.startDocument("UTF-8", true);
            serializer.startTag("", "element_list");
            for(XmlElement element: elements){
                serializer.startTag("", "element");

                    serializer.startTag("", "element_id");
                    serializer.text(String.valueOf(element.getID()));
                    serializer.endTag("", "element_id");

                    serializer.startTag("", "pos_x");
                    serializer.text(String.valueOf(element.getX()));
                    serializer.endTag("", "pos_x");

                    serializer.startTag("", "pos_y");
                    serializer.text(String.valueOf(element.getY()));
                    serializer.endTag("", "pos_y");

                    serializer.startTag("", "size");
                    serializer.text(String.valueOf(element.getSize()));
                    serializer.endTag("","size");

                serializer.endTag("", "element");

            }
            serializer.endTag("", "element_list");
            serializer.endDocument();
            serializer.flush();
            return writer.toString();
        } catch(Exception e){
            throw new RuntimeException(e);
        }
    }

    private static void saveStringToFile(String filepath,String xmlString){
        try{
            File file = new File(filepath);
            FileOutputStream fos = new FileOutputStream(file);
            OutputStreamWriter writer = new OutputStreamWriter(fos);
            writer.write(xmlString);
            writer.flush();
            writer.close();
            Log.d("XML: ","Wrote XML string successfully");
        } catch(FileNotFoundException e){
            Log.e("Error: " , e.getMessage());
        } catch(IOException e){
            Log.e("Error: ", e.getMessage());
        }
    }
}


