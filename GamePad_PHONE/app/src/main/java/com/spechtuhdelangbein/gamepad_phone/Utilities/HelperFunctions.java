package com.spechtuhdelangbein.gamepad_phone.Utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Environment;
import android.view.View;

import com.spechtuhdelangbein.gamepad_phone.Configuration.Configuration_Others;
import com.spechtuhdelangbein.gamepad_phone.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Constantin on 20.07.2015.
 */
public class HelperFunctions {

    /**
     * Converts dp into px
     * @param context context of calling function
     * @param dp density pixel input
     * @return Returns dp input as pixels
     */
    public static int dpToPixels(Context context, double dp) {
        final double density = context.getResources().getDisplayMetrics().density;
        return (int) Math.ceil(dp * density);
    }

    public static double pixelsToDp(Context context, int pixels) {
        final double density = context.getResources().getDisplayMetrics().density;
        return pixels/density;
    }

    /* Checks if external storage is available for read and write */
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public static Bitmap getBitmap(View v,int width,int height){
        Bitmap b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.draw(c);
        return b;
    }

    /**
     * Checks if the directory for layout files is existent, if not, creates the directory
     */
    public static void checkLayoutDirectory(Context context){
        File file=new File(Configuration_Others.layoutDirectory);
        if(!file.isDirectory()){
            file.mkdirs();
        }
        File thFile=new File(Configuration_Others.thumbnailDirectory);
        if(!thFile.isDirectory()){
            thFile.mkdirs();
        }
        File standardXml=new File(Configuration_Others.layoutDirectory+ Configuration_Others.layoutStandard);
        if (!standardXml.exists()){
            try {
                File layoutFile = new File(Configuration_Others.layoutDirectory + Configuration_Others.layoutStandard);
                InputStream in = context.getAssets().open("standard_layout.xml");
                OutputStream out = new FileOutputStream(layoutFile);

                byte[] buf = new byte[1024];
                int len;
                while((len=in.read(buf))>0){
                    out.write(buf,0,len);
                }
                in.close();
                out.close();
            }catch (FileNotFoundException e){
                e.printStackTrace();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
        File standardThb=new File(Configuration_Others.thumbnailDirectory+ Configuration_Others.thumbnailStandard);
        if (!standardThb.exists()){
            Bitmap standardBmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.standard_layout);
            saveThumbnail(standardBmp, Configuration_Others.standardName);
        }
    }

    public static boolean deleteLayout(String name){
        String bareName = name.substring(0,name.length()-4);
        File fileXml = new File(Configuration_Others.layoutDirectory+bareName+".xml");
        boolean deletedXml = fileXml.delete();
        File filePng = new File(Configuration_Others.thumbnailDirectory+bareName+".png");
        boolean deletedPng = filePng.delete();

        return deletedPng&&deletedXml;
    }

    public static void saveThumbnail(Bitmap image,String filename){
        FileOutputStream out = null;
        try{
            out = new FileOutputStream(Configuration_Others.thumbnailDirectory+filename+".png");
            image.compress(Bitmap.CompressFormat.PNG,100,out);
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                if(out !=null){
                    out.close();
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
}
