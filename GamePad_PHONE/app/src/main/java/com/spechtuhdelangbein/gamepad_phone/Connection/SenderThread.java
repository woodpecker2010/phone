package com.spechtuhdelangbein.gamepad_phone.Connection;

import android.content.Intent;
import android.util.Log;

import com.spechtuhdelangbein.gamepad_phone.Utilities.CommandHandler;

/**************************************************************************************************
 * This Thread is responsible for constantly sending the commands via the connection to the Tablet.
 * It's sending interval is determined by the value of SLEEP_TIME. I found out that it's not working
 * very well as the SLEEP_TIME is set to 0. So far an intervall of 1ms seems to be the best choice
 *
 * It needs the currently running CommandHandler (for sending it's command byte).
 *
 * Created by Benedikt on 15.06.2015.
 *************************************************************************************************/

public class SenderThread implements Runnable {

    /** time in ms between the sending intervals*/
    private final int SLEEP_TIME = 1;
    /** As soon as this variable is true, this sending thread stops it's work - forever*/
    private boolean stop = false;

    private CommandHandler commandHandler  = null;
    private ConnectedThread connectedThread = null;


    /**
     * Constructor initialized variables
     * @param ch Insert a CommandHandler which contains the commands
     * @param ct Insert a ConnectedThread which is used for sending
     */
    public SenderThread(CommandHandler ch, ConnectedThread ct){
        commandHandler = ch;
        connectedThread = ct;
    }

    /**
     * Constantly sending data packeges. May lead to an overflow or so
     */
    @Override
    public void run() {

        while(connectedThread.isConnected()){
            send();
            try{
                Thread.sleep(SLEEP_TIME);
            }catch (Exception e){}
        }
        Log.i("SenderThread","Sending stopped");
        Intent ReRunConnectionLobby = new Intent(connectedThread.mConLobby,ConnectionLobby.class);
        connectedThread.mConLobby.startActivity(ReRunConnectionLobby);

    }

    /**
     * stops the sending
      */
    public void stopSending(){
        stop = true;
        Log.i("SenderThread", "Stopping to send");
    }


    /**
     * Responsible so call commandHandlers send() method
     * @return True if successful.
     */
    private boolean send(){
        try {
            commandHandler.send(connectedThread);
        } catch (Exception e) {
            Log.e("Sending unsuccessful:",e.toString());
            return false;
        }
        return true;
    }
}
