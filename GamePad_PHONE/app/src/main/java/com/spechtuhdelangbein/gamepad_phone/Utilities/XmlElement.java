package com.spechtuhdelangbein.gamepad_phone.Utilities;

import java.util.List;

/**
 * Created by Constantin on 01.08.2015.
 *
 * Gamepad control element save format.
 * This XmlElement is given to the XmlParser as an array
 * The parser then saves the Elements array to a previously defined .xml file
 */
public class XmlElement {

    private int ID = 0;
    private double posX= 0,posY= 0,size= 0;

    public XmlElement(){
    }

    public XmlElement(int elementID, double pos_X,double pos_Y,double Size){

        this.ID=elementID;
        this.posX=pos_X;
        this.posY=pos_Y;
        this.size=Size;
    }

    public int getID(){
        return this.ID;
    }

    public void setID(int elementID){
        this.ID=elementID;
    }

    public double getX(){return this.posX;}

    public void setX(double pos_X){
        this.posX=pos_X;
    }

    public double getY(){
        return this.posY;
    }

    public void setY(double pos_Y){
        this.posY=pos_Y;
    }

    public double getSize(){
        return this.size;
    }

    public void setSize(double Size){
        this.size=Size;
    }

}
