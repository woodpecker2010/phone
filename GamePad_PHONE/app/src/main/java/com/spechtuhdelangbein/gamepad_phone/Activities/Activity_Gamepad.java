
package com.spechtuhdelangbein.gamepad_phone.Activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.spechtuhdelangbein.gamepad_phone.Configuration.Configuration_Global_Objects;
import com.spechtuhdelangbein.gamepad_phone.Configuration.Configuration_Others;
import com.spechtuhdelangbein.gamepad_phone.Utilities.CommandHandler;
import com.spechtuhdelangbein.gamepad_phone.Connection.ConnectedThread;
import com.spechtuhdelangbein.gamepad_phone.Connection.SenderThread;
import com.spechtuhdelangbein.gamepad_phone.Gamepad.GamepadElements;
import com.spechtuhdelangbein.gamepad_phone.Gamepad.GamepadListener;
import com.spechtuhdelangbein.gamepad_phone.R;

/**
 * Created by Constantin on 24.06.2015.
 */

public class Activity_Gamepad extends Activity{

    //create relative layout as base for the gamepad activity
    private RelativeLayout relLay;

    private ConnectedThread mConnectedThread;
    private CommandHandler commandHandler;
    private SenderThread senderThread;
    private int playerNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gamepad);

        // assign relative layout from XML to handle
        relLay=(RelativeLayout) findViewById(R.id.gamepad_relLay);

        // link the current connected thread and set up the Player Number
        mConnectedThread = Configuration_Global_Objects.mConnectedThread;
        playerNo         = mConnectedThread.receiveNewPlayerNo();
        mConnectedThread.setContext(this);

        // if no player Number was set up, close the activity and return to the ConnectionLobby
        if (playerNo == -1){
            String msg   = getResources().getString(R.string.error_receiving_player_number);
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
            this.finish();
        }
        Log.d("Activity_Gamepad","Player No is: " + playerNo);


        // Setup the commandHandler
        // and initialize it with the correct playerNumber and message type
        Configuration_Global_Objects.commandHandler = new CommandHandler();
        commandHandler = Configuration_Global_Objects.commandHandler;
        commandHandler.setPlayerNumber(playerNo);
        commandHandler.setNewMessageType(Configuration_Others.TYPE_GAMEPAD, playerNo);

        //create touch listener for all binary gamepad elements
        View.OnTouchListener touchListener = GamepadListener.createTouchListener(this, commandHandler);

        if (savedInstanceState == null){

            //Load sharedPrefs to get selected Layout
            SharedPreferences sharedPrefs = getSharedPreferences(Configuration_Others.LAYOUT_PREFS,0);
            //Set filepath to selected Layout
            String filepath =sharedPrefs.getString(Configuration_Others.SELECTED_LAYOUT, Configuration_Others.layoutStandard);
            //Fetch xml and create elements
            GamepadElements.loadElements(relLay, this, touchListener, filepath, commandHandler);
        }

        // create a thread that regularly sends commands
        // to the connected tablet
        senderThread = new SenderThread(commandHandler, mConnectedThread);
        Thread sender = new Thread(senderThread);
        sender.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(isFinishing()) {
            if (Configuration_Global_Objects.mConnectedThread != null) {
                Configuration_Global_Objects.mConnectedThread.cancel();
                Log.d("Milestone", "Closed socket from Activity_Gamepad");
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(isFinishing()) {
            if (Configuration_Global_Objects.mConnectedThread != null) {
                Configuration_Global_Objects.mConnectedThread.cancel();
                Log.d("Milestone", "Closed socket from Activity_Gamepad");
            }
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        // set the MessageType to the correct type as it may had been changed to Keyboard or Mouse
        commandHandler.setNewMessageType(Configuration_Others.TYPE_GAMEPAD, commandHandler.getPlayerNumber());
    }

}





